import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
  <h1>Saludos {{name}}!</h1>
  <h2>La hora y fecha es: {{tiempo}} </h2>
  <h2>Estas en {{so}}</h2>
  <h2>Navegas en: {{navegador}} {{mensaje}} </h2>
  <h2>EN: {{tiponav}} </h2>
  <h2>Cuya  version es: {{navVersion}} </h2>
  <h2>En la plataforma: {{navPlatform}}</h2>
  <h2>Tu posicion es Latitud: {{location.latitude}}, Longitud: {{location.longitude}} </h2>
  <div class=name>
      <h4>{{name}}</h4>
  </div>
  `,
  styles: [`
  h1{
    color: #370;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 300%;
  }
  h2{
    color: #369;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 200%;
  }

  .name{
    font-family: cursive
  }
  
  `]
})
export class AppComponent implements OnInit {
  name = 'Fernando Alberto Vásquez Canché';
  tiempo: Date;
  navegador: string = navigator.userAgent.split(')').reverse()[0].match(/(?!Gecko|Version|[A-Za-z]+?Web[Kk]it)[A-Z][a-z]+/g)[0];
  tiponav: string = navigator.userAgent.split(')').reverse()[1].match(/(?!Gecko|Version|[A-Za-z]+?Web[Kk]it)[A-Z][a-z]+/g)[0];
  so: string = navigator.userAgent.split(')').reverse()[2].match(/(?!Gecko|Version|[A-Za-z]+?Web[Kk]it)[A-Z][a-z]+/g)[1];
  navVersion: string = navigator.appVersion;
  navPlatform: string = navigator.platform;
  mensaje: string;
  
  location = {};
  setPosition(position){
    this.location = position.coords;
    console.log(this.location);
    setTimeout( position => this.location = position.coords, 0);
  }
  ngOnInit(){
    if(navigator.geolocation){
    navigator.geolocation.getCurrentPosition(this.setPosition.bind(this));
    };
  }

  
  constructor() {
    this.mensaje = navMensaje();
    this.tiempo = new Date();
    setInterval(() => this.tiempo = new Date(), 1000);


   function navMensaje(){
      var soSugenst: string = navigator.userAgent;
      var mensaje;
      if (soSugenst.indexOf('IE') !=-1) {
        mensaje = ', Por favor descargué otro navegador'
      } else if (soSugenst.indexOf('fox') !=-1) {
        mensaje = ', Nada extraordinario...'
      } else if (soSugenst.indexOf('Chro') !=-1) {
        if(soSugenst.indexOf('Chro') != -1){
          mensaje = ', Meh desperdicio de ram' 
        }else {
        mensaje = ', Nada que decir.'
      }
      return mensaje
    }


  }

}
