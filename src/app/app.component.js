"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var AppComponent = (function () {
    function AppComponent() {
        var _this = this;
        this.name = 'Fernando Alberto Vásquez Canché';
        this.navegador = navigator.userAgent.split(')').reverse()[0].match(/(?!Gecko|Version|[A-Za-z]+?Web[Kk]it)[A-Z][a-z]+/g)[0];
        this.tiponav = navigator.userAgent.split(')').reverse()[1].match(/(?!Gecko|Version|[A-Za-z]+?Web[Kk]it)[A-Z][a-z]+/g)[0];
        this.so = navigator.userAgent.split(')').reverse()[2].match(/(?!Gecko|Version|[A-Za-z]+?Web[Kk]it)[A-Z][a-z]+/g)[1];
        this.navVersion = navigator.appVersion;
        this.navPlatform = navigator.platform;
        this.location = {};
        this.mensaje = navMensaje();
        this.tiempo = new Date();
        setInterval(function () { return _this.tiempo = new Date(); }, 1000);
        function navMensaje() {
            var soSugenst = navigator.userAgent;
            var mensaje;
            if (soSugenst.indexOf('IE') != -1) {
                mensaje = ', Por favor descargué otro navegador';
            }
            else if (soSugenst.indexOf('fox') != -1) {
                mensaje = ', Nada extraordinario...';
            }
            else if (soSugenst.indexOf('Chro') != -1) {
                if (soSugenst.indexOf('Chro') != -1) {
                    mensaje = ', Meh desperdicio de ram';
                }
                else {
                    mensaje = ', Nada que decir.';
                }
                return mensaje;
            }
        }
    }
    AppComponent.prototype.setPosition = function (position) {
        var _this = this;
        this.location = position.coords;
        console.log(this.location);
        setTimeout(function (position) { return _this.location = position.coords; }, 0);
    };
    AppComponent.prototype.ngOnInit = function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.setPosition.bind(this));
        }
        ;
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n  <h1>Saludos {{name}}!</h1>\n  <h2>La hora y fecha es: {{tiempo}} </h2>\n  <h2>Estas en {{so}}</h2>\n  <h2>Navegas en: {{navegador}} {{mensaje}} </h2>\n  <h2>EN: {{tiponav}} </h2>\n  <h2>Cuya  version es: {{navVersion}} </h2>\n  <h2>En la plataforma: {{navPlatform}}</h2>\n  <h2>Tu posicion es Latitud: {{location.latitude}}, Longitud: {{location.longitude}} </h2>\n  <div class=name>\n      <h4>{{name}}</h4>\n  </div>\n  ",
            styles: ["\n  h1{\n    color: #370;\n    font-family: Arial, Helvetica, sans-serif;\n    font-size: 300%;\n  }\n  h2{\n    color: #369;\n    font-family: Arial, Helvetica, sans-serif;\n    font-size: 200%;\n  }\n\n  .name{\n    font-family: cursive\n  }\n  \n  "]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map